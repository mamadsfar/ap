package com.sfar.plato;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    Map<String, String> signUpInfo = new HashMap<>();
    EditText mUsernameView;
    EditText mPasswordView;
    EditText mConfirmPasswordEdit;
    TextView mConfirmPasswordView;
    TextView mNoUsername;
    TextView mUsernameAlreadyUsed;
    TextView mPasswordMustBeBetter;
    Button mSignUpButton;
    Button mLogInButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Intent logInIntent = new Intent(this, LogInActivity.class);
        mUsernameView = findViewById(R.id.username_sign_up);
        mPasswordView = findViewById(R.id.pass_sign_up);
        mConfirmPasswordEdit=findViewById(R.id.confirm_password_edit);
        mUsernameAlreadyUsed = findViewById(R.id.username_already_exist);
        mNoUsername=findViewById(R.id.please_enter_username);
        mPasswordMustBeBetter = findViewById(R.id.invalid);
        mConfirmPasswordView=findViewById(R.id.passwords_does_not_match_View);
        mSignUpButton = findViewById(R.id.sign_up_button);
        mLogInButton = findViewById(R.id.login_button);



        mLogInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(logInIntent);
            }
        });


        mUsernameView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mNoUsername.setVisibility(View.INVISIBLE);
                mUsernameAlreadyUsed.setVisibility(View.INVISIBLE);
                String password = mPasswordView.getText().toString();
                if (0 < password.length() && password.length() < 5) {
                    mPasswordMustBeBetter.setVisibility(View.VISIBLE);
                }
            }
        });


        mPasswordView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mPasswordMustBeBetter.setVisibility(View.INVISIBLE);
                String username = mUsernameView.getText().toString();
                boolean usernameIsInCorrect = false;
                signUpInfo=AccountsInfo.getSignUpInfo();
                for (String userName : signUpInfo.keySet()) {
                    if (username.equals(userName)) {
                        usernameIsInCorrect = true;
                        break;
                    }
                }
                if (usernameIsInCorrect) {
                    mUsernameAlreadyUsed.setVisibility(View.VISIBLE);
                }
            }
        });

        mConfirmPasswordEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mConfirmPasswordView.setVisibility(View.INVISIBLE);
            }
        });


        mSignUpButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String username = mUsernameView.getText().toString();
                boolean usernameIsInCorrect = false, passwordIsInCorrect = false;
                String password = mPasswordView.getText().toString();
                String confirm=mConfirmPasswordEdit.getText().toString();
                signUpInfo=AccountsInfo.getSignUpInfo();
                password.length();
                if (password.length() < 5) {
                    passwordIsInCorrect = true;
                }
                if (username.length()==0){
                    mNoUsername.setVisibility(View.VISIBLE);
                }
                else{
                for (String userName : signUpInfo.keySet()) {
                    if (username.equals(userName)) {
                        usernameIsInCorrect = true;
                    }
                }}
                if (passwordIsInCorrect||usernameIsInCorrect||!password.equals(confirm)||username.length()==0) {
                    if (!password.equals(confirm)){
                        mConfirmPasswordView.setVisibility(View.VISIBLE);
                }
                    if (passwordIsInCorrect){
                        mPasswordMustBeBetter.setVisibility(View.VISIBLE);
                    }
                    if (usernameIsInCorrect){
                        mUsernameAlreadyUsed.setVisibility(View.VISIBLE);
                    }
                }
                else{
                    AccountsInfo.addToMap(username,password);
                    startActivity(logInIntent);

                }
            }
        });


    }
}