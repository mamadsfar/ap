package com.sfar.plato;

import java.util.HashMap;
import java.util.Map;

public class AccountsInfo {
    static Map<String, String> signUpInfo = new HashMap<>();

    public static Map<String, String> getSignUpInfo() {
        return signUpInfo;
    }
    public static void addToMap(String k, String v){
        signUpInfo.put(k,v);
    }
}
