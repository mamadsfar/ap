package com.sfar.plato;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static com.sfar.plato.AccountsInfo.signUpInfo;

public class LogInActivity extends AppCompatActivity {
    EditText mUsernameView;
    EditText mPasswordView;
    TextView mInvalid;
    Button mSignUpButton;
    Button mLogInButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        final Intent signUpIntent=new Intent(this,MainActivity.class);
        mUsernameView= findViewById(R.id.username_log_in);
        mPasswordView= findViewById(R.id.pass_log_in);
        mInvalid= findViewById(R.id.invalid);
        mSignUpButton= findViewById(R.id.signUp_button);
        mLogInButton= findViewById(R.id.log_in_button);
        mSignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(signUpIntent);
            }
        });

        mLogInButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String username = mUsernameView.getText().toString();
                boolean usernameIsCorrect = false;
                String password = mPasswordView.getText().toString();
                signUpInfo=AccountsInfo.getSignUpInfo();
                for (String userName : signUpInfo.keySet()) {
                    if (username.equals(userName)||username.length()==0) {
                        usernameIsCorrect = true;
                        break;
                    }
                }
                if (usernameIsCorrect) {
                    if (password.equals(signUpInfo.get(username))){
                        AccountsInfo.addToMap(username,password);
                        mInvalid.setVisibility(View.INVISIBLE);
                        Toast.makeText(LogInActivity.this, "yaaaaaaaaay", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    mInvalid.setVisibility(View.VISIBLE);
                }
            }
        });
    }
}
